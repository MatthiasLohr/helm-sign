# Test Keys

This directory contains a keyring with two test keys:

| Key ID                                   | Passphrase | Remarks                     |
|------------------------------------------|------------|-----------------------------|
| 3DBB996D7FB10281C942A3F4D561C90DBD736806 | *None*     | Test key without passphrase |
| E1E4BAAC62DEC3708D2575BC62AECDC525329FAA | `S3cret!`  | Test key with passphrase    |
